
package pba.lab03.schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NationalityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NationalityType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PL"/&gt;
 *     &lt;enumeration value="DE"/&gt;
 *     &lt;enumeration value="UK"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "NationalityType")
@XmlEnum
public enum NationalityType {

    PL,
    DE,
    UK;

    public String value() {
        return name();
    }

    public static NationalityType fromValue(String v) {
        return valueOf(v);
    }

}
